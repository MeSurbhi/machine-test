import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:machinetest/shimmerEffect.dart';
import 'package:permission_handler/permission_handler.dart';
import 'Model/authorModel.dart';

class AuthorList extends StatefulWidget {
  @override
  _AuthorListState createState() => _AuthorListState();
}

class _AuthorListState extends State<AuthorList> {
  List<AuthorModelClass> list = [];
  List<AuthorModelClass> listAdd = [];

  ScrollController _scrollController = ScrollController();
  final Dio dio = Dio();
  int pageNumber = 1;
  var selectedIndex;

  bool loading = false;
  bool _loading = false;

  double progress = 0;

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  //======================= Rest Api ======================

  Future<List<AuthorModelClass>> getData() async {
    String link = 'https://picsum.photos/v2/list?page=$pageNumber';
    print(link);
    try {
      var res = await http.get(
        Uri.parse(link),
        headers: {
          "Accept": "application/json",
        },
      );

      if (res.statusCode == 200) {
        var data = json.decode(res.body);
        print(data);
        var rest = data as List;
        setState(() {
          listAdd = rest
              .map<AuthorModelClass>((json) => AuthorModelClass.fromJson(json))
              .toList();
          list.addAll(listAdd);
          _loading = false;
        });
        print(list.length);
      }
    } on SocketException {}

    return list;
  }

  //======================= Storage Permission ======================

  Future<Null> _prepare() async {
    final status1 = await Permission.storage.request();
    final status2 = await Permission.manageExternalStorage.request();

    if (status1.isGranted || status2.isGranted) {
    } else {}
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
    _prepare();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        pageNumber = pageNumber + 1;
        print("pageNumber");
        print(pageNumber);
        getData();
        setState(() {
          _loading = true;
        });
      }
    });
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Author List"),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        child: (list.length == 0)
            ? LoadingListPage()
            : Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      controller: _scrollController,
                      itemCount: list == [] ? 0 : list.length,
                      itemBuilder: (BuildContext context, int position) {
                        Text(
                          list.length.toString(),
                        );

                        return Container(
                          margin: const EdgeInsets.symmetric(
                            vertical: 10.0,
                            horizontal: 10.0,
                          ),
                          child: Card(
                            elevation: 8,
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 15.0),
                              child: ListTile(
                                  leading: ClipRRect(
                                    borderRadius:
                                        BorderRadius.circular(20000.0),
                                    child:
                                        /*CachedNetworkImage(
                                    imageUrl: '${list[position].downloadUrl}',
                                    placeholder: (context, url) => CircleAvatar(
                                      radius: 30,
                                      backgroundColor: Colors.grey[300],
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                                ),*/
                                        CircleAvatar(
                                      radius: 30,
                                      backgroundImage: NetworkImage(
                                          '${list[position].downloadUrl}?auto=compress&cs=tinysrgb&dpr=1&w=200'),
                                      backgroundColor: Colors.transparent,
                                    ),
                                  ),
                                  title: Text(list[position].author),
                                  trailing: GestureDetector(
                                      onTap: () async {
                                        setState(() {
                                          setState(() {
                                            selectedIndex = position;
                                          });
                                        });
                                        await downloadFile(
                                            list[position].downloadUrl,
                                            list[position].author);
                                      },
                                      child: selectedIndex == position
                                          ? loading
                                              ? CircularProgressIndicator()
                                              : Icon(Icons.download_rounded)
                                          : Icon(Icons.download_rounded))),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Container(
                    height: _loading ? 50.0 : 0,
                    color: Colors.transparent,
                    child: Center(
                      child: new CircularProgressIndicator(),
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  //===========================Download Image=======================================

  Future<bool> saveImage(String url, String fileName) async {
    Directory directory;
    try {
      if (Platform.isAndroid) {
        if (await _requestPermission(
          Permission.storage,
        )) {
          directory = (await getExternalStorageDirectory())!;
          String newPath = "";
          print(directory);
          List<String> paths = directory.path.split("/");
          for (int x = 1; x < paths.length; x++) {
            String folder = paths[x];
            if (folder != "Android") {
              newPath += "/" + folder;
            } else {
              break;
            }
          }
          newPath = newPath + "/MachineTest";
          directory = Directory(newPath);
        } else {
          return false;
        }
      } else {
        if (await _requestPermission(Permission.storage)) {
          directory = await getApplicationDocumentsDirectory();
          print('directory');
          print(directory);
        } else {
          return false;
        }
      }
      File saveFile = File(directory.path + "/$fileName.jpeg");
      if (!await directory.exists()) {
        await directory.create(recursive: true);
      }
      if (await directory.exists()) {
        await dio.download(url, saveFile.path,
            onReceiveProgress: (value1, value2) {
          setState(() {
            progress = value1 / value2;
          });
        });
        if (Platform.isIOS) {
          await dio.download(url, saveFile.path,
              onReceiveProgress: (value1, value2) {
            setState(() {
              progress = value1 / value2;
            });
          });
        }
        return true;
      }
      return false;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> _requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  downloadFile(var url, author) async {
    setState(() {
      loading = true;
      progress = 0;
    });
    bool downloaded = await saveImage(url, author);
    if (downloaded) {
      _showNotification(author);
      print("Image Downloaded");
    } else {
      print("Problem Downloading File");
    }
    setState(() {
      loading = false;
    });
  }

  //=========================== Showing Local Notification =======================================

  Future<void> _showNotification(String author) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'channel id', 'channel name', 'channel description',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker');
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, 'Image Successfully Downloaded', '$author', platformChannelSpecifics,
        payload: 'item x');
  }
}
