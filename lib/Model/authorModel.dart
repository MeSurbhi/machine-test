class AuthorModelClass {
 late String id;
 late String author;
 late int width;
 late int height;
 late String url;
 late String downloadUrl;

 AuthorModelClass(
      {required this.id,
        required this.author,
        required this.width,
        required this.height,
        required this.url,
        required this.downloadUrl});

 AuthorModelClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    author = json['author'];
    width = json['width'];
    height = json['height'];
    url = json['url'];
    downloadUrl = json['download_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['author'] = this.author;
    data['width'] = this.width;
    data['height'] = this.height;
    data['url'] = this.url;
    data['download_url'] = this.downloadUrl;
    return data;
  }
}


